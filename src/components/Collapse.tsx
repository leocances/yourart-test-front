import { hover } from "@testing-library/user-event/dist/hover";
import React from "react";

import { IoMdArrowDropdown, IoMdArrowDropup } from "react-icons/io";

interface CollapseProps {
  title: string;
  children: React.ReactNode;
}


const Collapse: React.FC<CollapseProps> = (props) => {
  const [open, setOpen] = React.useState(false);
  const [hover, setHover] = React.useState(false);

  const collapseStyle={
    display: 'flex',
    background: hover ? '#f5f5f5' : '',
    padding: '8px 16px',
    cursor: 'pointer',
    borderBottom: '1px solid #e0e0e0',
  }

  return (
    <div>
      <div 
        style={collapseStyle}
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
        onClick={() => setOpen(!open)}
        
      >
        <h4>{props.title}</h4> 
      </div>
      {open && props.children}
    </div>
  )
}

export default Collapse;