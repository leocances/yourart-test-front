import React from "react";
import { GiSandsOfTime, GiCheckMark, GiTruck} from "react-icons/gi";
import { FaMapMarkerAlt } from "react-icons/fa";
import { AiOutlineStar, AiFillStar } from "react-icons/ai";

import { Dimensions } from "../types";


const StartIt: React.FC = () => {
  const [toggle, setToggle] = React.useState(false);

  return (
    <div style={{cursor: 'pointer'}} onClick={() => setToggle(t => !t)}>
    { toggle ? <AiOutlineStar size={25}/> : <AiFillStar size={25}/> }
    </div>
  )
}


interface ArtInfoProps {
  title: string;
  artistFullName: string;
  country: string;
  category: string;
  creationYear: number;
  dimensions: Dimensions;
  price: number;
}

const ArtInfo: React.FC<ArtInfoProps> = (props) => {
  const titleStyle = {fontSize: '24px', fontWeight: 'bold', margin: '0px'}
  const artistStyle = {color: 'orange', fontWeight: 'bold', margin: '0px'};
  const countryStyle = {fontWeight: 'bold', margin: '0 0 16px 16px'};
  const priceStyle = {fontSize: '1.5em', fontWeight: 'bold'};
  const inputStyle = {padding: '8px', width: '100%'}

  const buttonBlackStyle = {
    background: 'black',
    color: 'white',
    border: '1px solid black',
    padding: '10px 20px',
    borderRadius: '1000px',
    width: '100%',
    margin: '10px 0',
    fontWeight: 'bold',
    fontSize: '1.2em',
    cursor: 'pointer'
  }

  const buttonWhiteStyle = {
    background: 'white',
    color: 'black',
    border: '1px solid black',
    padding: '10px 20px',
    borderRadius: '1000px',
    width: '100%',
    fontWeight: 'bold',
    fontSize: '1.2em',
    cursor: 'pointer'
  }

  // clean category and title display
  const country = props.country.charAt(0).toUpperCase() + props.country.slice(1)
  let category = props.category.toLowerCase()
  category = category.charAt(0).toUpperCase() + category.slice(1)

  return (
    <div style={{width: '316px', marginLeft: '32px', textAlign: 'left'}}>
      <div style={{display: 'flex', width: '100%'}} >
        <p style={{...titleStyle, marginRight: 'auto'}}>{props.title}</p>
        <StartIt />
      </div>

      <div style={{display: 'flex'}}>
        <p style={artistStyle}>{props.artistFullName}</p>
        <p style={countryStyle}>{country || 'France'}</p>
      </div>

      <div>
        <p>{category}, {props.creationYear}</p>
        <p>{props.dimensions.width} W x {props.dimensions.height} H x {props.dimensions.depth} D in</p>
      </div>

      <div style={{display: 'flex'}}>
        <p style={priceStyle}>{props.price} $</p>
      </div>

      <div>
        <button style={buttonBlackStyle}>Acquire</button>
        <button style={buttonWhiteStyle}>Make an offer</button>
      </div>

      <div >
        <p><GiSandsOfTime /> PRE-RESERVE FOR 24 HOURS</p>
      </div>

      <div style={{display: 'flex', fontSize: 'small'}}>
        <p><GiCheckMark /><strong>131$ estimated delivery fee</strong> for France</p>
      </div>

      <div>
        <p style={{fontWeight: 'bold'}}>
          In order to obtain accurate delivery fee, please enter your country or residence and zip code:
        </p>
        <div style={{display: 'flex'}}>
          <select value={'france'} style={inputStyle}>
            <option value="france">France</option>
            <option value="spain">Spain</option>
            <option value="united-states">United States</option>
          </select>

          {/* <input style={inputStyle} type="text" id="country" name="country" placeholder="SPAIN"/> */}
          <input style={{...inputStyle, margin: '0px 8px'}} type="text" id="zip" name="zip" placeholder="81932"/>
        </div>
      </div>

      <div style={{textAlign: 'left'}}>
        <p><GiTruck /> Delivery fee is 129$</p>
        <p><FaMapMarkerAlt />Free pickup in <strong>Bruxelle, Belgium</strong></p>
        <p><GiCheckMark /> Try 14 days at home for free</p>
      </div>

    </div>
  )
}

export default ArtInfo;