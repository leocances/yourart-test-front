import React from "react";

import { BiChevronLeft, BiChevronRight } from "react-icons/bi";

interface CarrouselProps {
  otherArtworkImages: string[];
}


const Carrousel: React.FC<CarrouselProps> = (props) => {
  const {otherArtworkImages} = props;

  // The chevron should be clickable and change the images displayed
  const imageWidth = 128;
  const chevronStyle = {
    width: `${imageWidth}px`,
    padding: "8px",
    margin: "auto auto",
    cursor: "pointer",
  };
  const carrouselStyle = { 
    display: 'flex',
    justifyContent: 'space-between',
    overflowY: 'hidden',
    overflowX: 'hidden',
    userSelect: 'none'
  };

  const nb_images = otherArtworkImages.length;
  const [carrousePos, setCarrousePos] = React.useState(0);
  const ref = React.useRef<HTMLDivElement>(null);

  const leftMoveHandler = () => {
    if (carrousePos > 0) {
      setCarrousePos(carrousePos - 1);
      ref.current && ref.current.scrollBy({left: -imageWidth, top: 0, behavior: 'smooth'})
    }
  }

  const rightMoveHandler = () => {
    if (carrousePos < nb_images - 1) {
      setCarrousePos(carrousePos + 1);
      ref.current && ref.current.scrollBy({left: imageWidth, top: 0, behavior: 'smooth'})
    }
  }


  return (
    <div style={{ display: 'flex'}}>
      
      <div style={chevronStyle}>
        {carrousePos > 0 && (
          <BiChevronLeft size={70} onClick={leftMoveHandler} />
        )}
      </div>

      <div ref={ref} style={{ 
        display: 'flex',
        justifyContent: 'space-between',
        overflowY: 'hidden',
        overflowX: 'hidden',
        userSelect: 'none'
      }}>
        {
          otherArtworkImages.map((imageUrl, index) => {
            return (
              <img src={imageUrl} alt={imageUrl} key={index}
                width={`${imageWidth}px`} height='128px' style={{ margin: '8px' }} />
            )
          })
        }
      </div>

      <div style={chevronStyle}>
        {carrousePos < nb_images && (
          <BiChevronRight size={70} onClick={rightMoveHandler} />
        )}
      </div>

    </div>
)
}

export default Carrousel;