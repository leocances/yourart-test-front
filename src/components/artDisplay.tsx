import React from "react";

import ImageDisplay from './ImageDisplay';
import Collapse from './Collapse';


interface ArtDisplayProps {
  src: string;
  description: string;
  mediums: string[];
  styles: string[];
  subjects: string[];
  materials: string[];
}


const ArtDisplay: React.FC<ArtDisplayProps> = (props) => {
  const rowStyle = {height: '32px'}
  const firstColumnStyle = {fontWeight: 'bold'}
  return (
    <div style={{ width: "60%"}} >
      <ImageDisplay
        src={props.src}
      />

      <div style={{ textAlign: 'left'}}>
        <Collapse title="DESCRIPTION">
          <p>{props.description}</p>
        </Collapse>

        <Collapse title="SUBJECT, MEDIUM, STYLE, MATERIALS">
        <table style={{textAlign: 'left'}}>
          <tbody>
            <tr style={rowStyle}>
              <td style={firstColumnStyle}>SUBJECT</td>
              <td>{props.subjects.join(', ')}</td>
            </tr>
            <tr style={rowStyle}>
              <td style={firstColumnStyle}>STYLE</td>
              <td>{props.styles.join(', ')}</td>
            </tr>
            <tr style={rowStyle}>
              <td style={firstColumnStyle}>MEDIUM</td>
              <td>{props.mediums.join(', ')}</td>
            </tr>
            <tr style={rowStyle}>
              <td style={firstColumnStyle}>MATERIALS</td>
              <td>{props.materials.join(', ')}</td>
            </tr>
          </tbody>
          </table>
        </Collapse>
      </div>
    </div>
  )
}


export default ArtDisplay;