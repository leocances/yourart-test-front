import path from "path";
import React from "react";


const BreadCrumb: React.FC<{ path: string[] }> = ({ path }) => {
  const noUnderlineStyle= {textDecoration: 'none'}
  const pathStyle = {color: 'gray', fontWeight: 'bold', ...noUnderlineStyle}
  const finalPathStyle = {color: 'black', fontWeight: 'bold', ...noUnderlineStyle}
  const separatorStyle = {color: 'gray', margin: '0 5px'}
  const containerStyle = {display: 'flex', margin: '10px 0', height: '32px', marginBottom: '32px'}

  return (
    <div style={containerStyle} >
      {path.map((p, i) => (
        <span key={i}>
          <a href='#' style={ i < path.length - 1 ? pathStyle : finalPathStyle}>
            {p}
          </a>
          {i < path.length - 1 && <span style={separatorStyle}>{'>'}</span>}
        </span>
      ))}
    </div>
  );
}

export default BreadCrumb;