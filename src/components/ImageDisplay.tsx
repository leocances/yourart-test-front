import React from "react";

import { FaEye } from "react-icons/fa";
import { TbAugmentedReality } from "react-icons/tb";


interface ImageDisplayProps {
  src: string;
}


const ImageDisplay: React.FC<ImageDisplayProps> = (props) => {
  const aStyle = {color: 'black', textDecoration: 'none'}

  return (
    <div>
      <div className="image-display">
        <img src={props.src} alt="Snow" height={'auto'} width={'100%'}/>      
      </div>
      
      <div>
        <div style={{marginTop: '8px'}}>
          <FaEye />
          <a style={aStyle} href='#'><span style={{margin: '0 64px 64px 16px' }}>VIEW IN A ROOM</span></a>

          <TbAugmentedReality />
          <a style={aStyle} href='#'><span style={{margin: '0 16px'}}>AR VIEW</span></a>
        </div>
      </div>

    </div>
  );
}

export default ImageDisplay;