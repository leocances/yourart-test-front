import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Routes
} from "react-router-dom";

import './App.css';

import ArtworkPage from './pages/artwork';



function App() {

  React.useEffect(() => {
    // Fetch the information using the API

  }, []);

  return (
    <Router>
      <Routes>
        <Route path="/artwork/:id" element={<ArtworkPage />} />
      </Routes>
    </Router>
  );
}

export default App;
