import React from "react";
import { useParams } from "react-router-dom";

import BreadCrumb from '../components/breadcrumb';
import ArtDisplay from '../components/artDisplay';
import ArtInfo from "../components/artInfo";
import Carrousel from "../components/carrousel";
import { ArtworkInfoType } from '../types';

// # FIXME: Couldn't connect to the google storage because of CORS
// 'https://storage.googleapis.com/ya-misc/interviews/front/examples/';
const STORAGE_ROOT_URL =  '/data/'


const ArtworkPage: React.FC = () => {
  const { id } = useParams();
  const [artworkInfo, setArtworkInfo] = React.useState<ArtworkInfoType>();

  React.useEffect(() => {
    const url = `${STORAGE_ROOT_URL}${id}.json`;
    fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then((res) => res.json())
    .then((data) => {
      setArtworkInfo(data);
    })
    .catch((err) => {
      console.error(err);
    });
  }, []);

  return (
    <div className="App" style={{padding: '16px'}}>
      { artworkInfo ? (
        <div style={{maxWidth: '1200px', alignContent: 'center', marginLeft: 'auto', marginRight:'auto'}}>

          <BreadCrumb path={["Home", "Painting", "Madeleine Eister  Artworks", `${artworkInfo.title}`]} />

          <div style={{display: 'flex', marginBottom: '32px'}}>
            <ArtDisplay
              src={artworkInfo.imageUrl}
              description={artworkInfo.description}
              mediums={artworkInfo.mediums}
              styles={artworkInfo.styles}
              subjects={artworkInfo.subjects}
              materials={artworkInfo.materials}
            />

            <ArtInfo
              title={artworkInfo.title}
              artistFullName={artworkInfo.artistShort.fullname}
              country={artworkInfo.artistShort.country}
              category={artworkInfo.category}
              creationYear={artworkInfo.creationYear}
              dimensions={artworkInfo.dimensions}
              price={artworkInfo.price}
            />
          </div>

          <Carrousel otherArtworkImages={artworkInfo.otherArtworkImages} />
        </div>
      ) : (
        <div>Loading...</div>
      )
      }
      
    </div>
  )
}

export default ArtworkPage;
