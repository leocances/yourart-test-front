export interface Dimensions {
    depth: number;
    height: number;
    width: number;
}


export interface ArtworkInfoType {
    _id: string;
    title: string;
    category: string;
    styles: string[];
    mediums: string[];
    materials: string[];
    subjects: string[];
    description: string;
    dimensions: Dimensions;
    creationYear: number;
    imageUrl: string;
    price: number;
    artistShort: {
        name: string;
        country: string;
        countryCode: string;
        fullname: string;
    };
    fullname: string;
    status: string;
    artistId: string;
    otherArtworkImages: string[];
}