# YouArt test front

L’idée est de réaliser la page suivante en React.js.
C’est la page d’une artwork (en fait c’est l’extrait d’une page d’artwork chez nous).
La donnée vient de là :
- https://storage.googleapis.com/ya-misc/interviews/front/examples/1.json
- https://storage.googleapis.com/ya-misc/interviews/front/examples/0.json

L’url doit être du type domain/artwork/x (e.g. http://localhost:7000/artwork/0) où x vaut 0 ou 1 et en fonction de cela on vient chercher via un appel sur Google Storage (avec les liens ci-dessus) l’artwork 0 ou 1, puis afficher ce qu’il faut. Les champs du json sont self-explanatory. Certains éléments de la page sont arbitraires (essentiellement ce qui a trait au delivery fee/pays) donc à mettre en dur dans la page. A la fin, le carrousel d’images vient du champ “otherArtworkImages”.
Si certains points ne sont pas explicités, l’idée est que tu fasses comme cela te semble le plus naturel. Néanmoins, n’hésite pas à dire dans ton livrable les questions que tu aurais pu avoir.

Bon courage !


# Installation and running
```bash
yarn install
yarn dev
```

## Display
look for url:
- http://localhost:3000/artwork/0
- http://localhost:3000/artwork/1